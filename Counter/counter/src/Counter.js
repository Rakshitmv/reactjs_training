import React, { useState, useEffect } from 'react'
import './Counter.css'

const Counter = () => {

  const [num, setNum] = useState(0);

  const handleClickInc = () => {
    setNum(num + 1);
  };

  const handleClickDec = () => {
    setNum(num - 1);
  };

  const handleClickRes = () => {
    setNum(0);
  };

  const handleClickInncreament = (n) => {
    setNum(num + n)
  };

  const handleClickDecreament = (n) => {
    setNum(num - n);
  };


  return (
    <>
      <div className='container'>
        <input type='text' onChange={setNum} value={num}>
        </input>

        <div className='buttons'>
          <button onClick={handleClickInc}>Increament</button>
          <button onClick={handleClickDec}>Decreament</button>
          <button onClick={handleClickRes}>Reset</button>
          <button onClick={() => handleClickInncreament(5)}>Increament by n</button>
          <button onClick={() => handleClickDecreament(5)}>Decreament by n</button>
        </div>
      </div>
    </>
  )
}

export default Counter