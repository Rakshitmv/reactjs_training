import React from 'react';
import Login from './components/Login';
import './App.css'
import { ThemeProvider, useTheme, createContext, useContext } from './components/ThemeContext';



function App() {

  // const { darkMode, toggleDarkMode } = useTheme();
  return (
    <>
      <ThemeProvider>
        <div>

          <Login />
        </div>
      </ThemeProvider>


    </>
  );
}

export default App;
