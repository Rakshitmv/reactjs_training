import React from 'react'
import axios from 'axios'



export const apiClient = axios.create();

apiClient.interceptors.request.use(
  (config) => {
    config.headers["accept-language"] = "en";
    config.headers["x-api-key"] = "1ab2c3d4e5f61ab2c3d4e5f6";

    console.log(config + "  " + "request accept headers");
    return config;
  },
  (error) => {
    console.log(error);
    Promise.reject(error);
  }
);


apiClient.interceptors.response.use(
  (response) => {

    return response;
  },
  (error) => {
    return Promise.reject(error);
  }

);
