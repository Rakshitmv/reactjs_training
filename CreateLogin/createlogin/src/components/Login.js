import React from 'react'
import { useState, useEffect } from 'react'
import axios from 'axios'
import { useTheme } from './ThemeContext.js'
import { apiClient } from './AxiosInterceptors.js'

const Login = () => {

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [token, setToken] = useState(null)
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const { darkMode, toggleDarkMode } = useTheme();
  const [theme, setTheme] = useState('light');


  const handleEmail = (e) => {
    setEmail(e.target.value)
  }

  const handlePassword = (e) => {
    setPassword(e.target.value)
  }


  const handleApi = (e) => {
    e.preventDefault();
    console.log(email, password)

    const data = {
      email: email,
      password: password,
      remember_me: true
    }
    apiClient.post('https://kgk-api.kgk.magnetoinfotech.com/v1/auth/sign-in', data)
      .then((response) => {
        console.log(response.data);
        localStorage.setItem('access_token', response.data.data.access_token)
        console.log("set Token " + response.data.data.access_token);
        setIsLoggedIn(true);
      })

      .catch((error) => {
        console.log(error);
      });
  }

  useEffect(() => {
    const token = localStorage.getItem('access_token')
    setToken(token);

    console.log("get token" + token);

  }, []);


  useEffect(() => {
    if (token != null) {
      // alert("user login");
    }
  }, [token]);



  const handleLogout = () => {

    localStorage.removeItem('access_token');
    setToken(null);
    setIsLoggedIn(false);
  }


  // console.log('dark mode : ', darkMode);


  return (
    <div className={darkMode ? "darkMode" : ""}>
      {isLoggedIn ? (
        <>
          <p>User logged in successfully!</p>
          <button onClick={handleLogout}>Logout</button>


        </>
      ) : (
        <>

          <button onClick={() => toggleDarkMode()}>
            {darkMode ? 'Light Mode' : 'Dark Mode'}
          </button>

          <form>
            <label>Email   </label>
            <input type='text' name='email' value={email} onChange={handleEmail}></input><br />
            <label>Password   </label>
            <input type='password' name='password' value={password} onChange={handlePassword}></input><br></br>
            <button type="submit" onClick={handleApi}>Submit</button>
          </form>

        </>
      )}
    </div>
  )
}

export default Login