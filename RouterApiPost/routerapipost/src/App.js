import './App.css';
import React, { useState, useEffect } from 'react';
import { BrowserRouter, Route, Link, Routes } from 'react-router-dom';
import Posts from './component/Posts';
import PostDetail from './component/PostDetail';
import About from './component/About';
import Login from './component/Login';
import Navbar from './component/Navbar';
import HomeHeader from './component/HomeHeader';
import Error from './component/Error';
import Home from './component/Home';

// import routerapipost from "../routerapipost";

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<HomeHeader />} >
            <Route index element={<Home />} />
            <Route path="/post" element={<Posts />} />
            <Route path="/post/:id" element={<PostDetail />} />
            <Route path="/about" element={<About />} />
            <Route path="/login" element={<Login />} />

          </Route>
          <Route path='*' element={<Error />} />
        </Routes>
      </BrowserRouter>

    </>
  );
}

export default App;
