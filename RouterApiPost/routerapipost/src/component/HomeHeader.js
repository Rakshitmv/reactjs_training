import React from 'react'
import { NavLink, Outlet } from 'react-router-dom'
import Navbar from './Navbar'

const HomeHeader = () => {
  return (
    <>
      <div>
        <Navbar />
      </div>
      <Outlet />
    </>
  )
}

export default HomeHeader