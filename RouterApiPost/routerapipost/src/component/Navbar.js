import React from 'react'
import { NavLink } from 'react-router-dom'

const Navbar = () => {
  return (
    <>

      <nav>
        <ul>
          <li>
            <NavLink to="/post">Posts</NavLink>
          </li>
          <li>
            <NavLink to="/about">About</NavLink>
          </li>
          <li>
            <NavLink to="/login">Login</NavLink>
          </li>

        </ul>
      </nav>
    </>
  )
}

export default Navbar