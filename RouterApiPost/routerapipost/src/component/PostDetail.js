import React from 'react'
import { useEffect, useState, } from 'react';
import { useParams } from 'react-router-dom';


const PostDetail = ({ match }) => {

  // const postId = match.params.id;
  const [post, setPost] = useState(null);


  let { id } = useParams();



  useEffect(() => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
      .then((response) => response.json())
      .then((data) => setPost(data));
  }, [id]);


  return (
    <>
      <div>
        <h1>Post Detail</h1>
        {post ? (
          <div>
            <h2>{post.title}</h2>
            <p>{post.body}</p>
          </div>
        ) : (
          <p>Loading...</p>
        )}
      </div>
    </>
  )
}

export default PostDetail