import React, { useEffect, useRef } from 'react';

function SearchInput() {
  const searchInputRef = useRef(null);

  useEffect(() => {

    searchInputRef.current.focus();
  }, []);

  return (
    <div>
      <h1>Search Page</h1>
      <form action="#" method="get">
        <input
          type="text"
          name="Rakshit"
          placeholder="Search..."
          ref={searchInputRef}
        />
        <button type="submit">Search</button>
      </form>
    </div>
  );
}

export default SearchInput;
